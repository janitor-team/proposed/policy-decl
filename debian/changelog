policy-rcd-declarative (0.7) UNRELEASED; urgency=medium

  * policy-rc.d-declarative.{prerm,postrm}: fix path to binary. It is
    installed in /usr/sbin, not /usr/bin. Silence lintian a bit.

 -- Wouter Verhelst <wouter@debian.org>  Wed, 19 May 2021 12:47:32 +0200

policy-rcd-declarative (0.6) unstable; urgency=medium

  * Make the -allow-all and -deny-all packages recommend, rather than
    depend, the base package, to avoid dependency loops.
    Closes: #971366, #975080.

 -- Wouter Verhelst <wouter@debian.org>  Mon, 23 Nov 2020 09:58:08 +0200

policy-rcd-declarative (0.5) unstable; urgency=medium

  * Now that we passed NEW, reupload as source-only upload to allow for
    testing migration.

 -- Wouter Verhelst <wouter.verhelst@zetes.com>  Mon, 02 Nov 2020 17:05:58 +0200

policy-rcd-declarative (0.4) unstable; urgency=medium

  * Split out the package into a "policy-rcd-declarative-allow-all" and
    "policy-rcd-declarative" package, and add a
    "policy-rcd-declarative-deny-all" package that provides an alternate
    default policy (denying all requests for service startup).
    Closes: #970027

 -- Wouter Verhelst <wouter@debian.org>  Thu, 10 Sep 2020 15:38:03 +0200

policy-rcd-declarative (0.3) unstable; urgency=medium

  * Add "clean" override, so that we don't ship pre-built files
  * Avoid hardcoding Perl RE syntax, and use re::engine::RE2 as regex
    engine, so that we can switch to another language easily in the
    future should we want to.
  * Upload to unstable

 -- Wouter Verhelst <wouter@debian.org>  Tue, 29 Oct 2019 18:49:29 +0200

policy-rcd-declarative (0.2) experimental; urgency=medium

  * Fix lintian errors:
    - Fix versioned build-dependency on debhelper from 9 to 10
    - Convert to 3.0 (native) source format
    - Add "set -e" to all maintainer scripts
    - Build-Depend on perl, not perl-modules
  * Add Vcs-* headers
  * Remove the alternative in case "postrm remove" or "postrm disappear"
    is called, and add a missing space in the "prerm remove" [] test.
    Closes: #927448.

 -- Wouter Verhelst <wouter@debian.org>  Thu, 02 May 2019 11:31:23 +0200

policy-rcd-declarative (0.1) experimental; urgency=medium

  * Initial release.

 -- Wouter Verhelst <wouter@debian.org>  Mon, 11 Mar 2019 00:35:02 +0200
