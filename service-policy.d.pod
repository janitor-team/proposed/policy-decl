=head1 NAME

/etc/service-policy.d/*.pol - configuration for policy-rc.d-declarative

=head1 SYNOPSIS

F</etc/service-policy.d/*.pol>

=head1 DESCRIPTION

Files in the directory F</etc/service-policy.d> are opened and parsed by
the F<policy-rc.d-declarative> script. Lines should have exactly three
fields of data which are separated by whitespace; any trailing
whitespace is ignored.

All files are opened in order by file name; following that, all rules in
a file are evaluated in order.

The first rule that matches a given service with a given action is
applied. All further rules are ignored.

If no rule matches a given service with a given action, the default is
to exit with exit state 100, which signals "unknown initscript id" to
the calling update-rc.d program.

=head1 FIELDS

The file contains the following fields:

=over 4

=item 1.

The service(s) to which this should apply. This should be a perl regular
expression that matches the init scripts for which this policy should be
used.

=item 2.

The action(s) which F<invoke-rc.d> wants to take. For more information
on the available actions, see
F</usr/share/doc/init-system-helpers/README.policy-rc.d.gz>.

=item 3.

The policy to apply to this action. Available policies are:

=over 4

=item allow

Allow the action that invoke-rc.d wants to perform without modification

=item deny

Do not allow the action.

=item restart-ignore

Replace the action that invoke-rc.d wants to perform by C<restart stop>.
That is, try a restart action first; if that fails for some reason, try
a stop action.

Note that at the time of this writing, not all PID1 implementations
support this. Notably, the systemd version of invoke-rc.d does not
support overriding actions.

=back

=back

=head1 SEE ALSO

F</usr/share/doc/init-system-helpers/README.policy-rc.d.gz>,
L<policy-rc.d-declarative(1)>
